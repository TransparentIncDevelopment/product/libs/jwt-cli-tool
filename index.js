#!/usr/bin/env node

var program = require('commander'),
    jsonfile = require('jsonfile'),
    fileExists = require('file-exists'),
    jwt = require('jwt-simple'),
    duration = require('parse-duration'),
    base64url = require('base64-url');

program
    .version('1.0.0')
    .usage('<secret> [options]')
    .option('-e, --email <email>', 'Email address associated with token')
    // .option('-a, --audience <audience>', 'Audience')
    // .option('-d, --domain <domain>', 'Domain')
    .option('-x, --expires [duration]', 'Time to live for token. 1s, 3w, 2y, etc. Defaults to 1d (1 day).')
    .parse(process.argv);


if (!fileExists(program.file)) {
    console.log("The file '%j' does not exist.", program.file);
    process.exit(1);
}

if (!program.args[0]) {
    console.log("The subject '%j' was not defined.", program.args[0]);
    process.exit(1);
}

// if (!program.secret || program.secret.length === 0) {
//     console.log("Invalid secret");
//     process.exit(1);
// }

var timestamp = Math.round(new Date().getTime() / 1000);
var expires = Math.round(duration(program.expires || '1y') / 1000);

var payload = {
    // "iss": program.domain,
    "sub": ("google-apps|" + program.args[0]),
    "email": program.email,
    // "aud": program.audience,
    "exp": (timestamp + expires),
    "iat": timestamp
};

var secret = new Buffer(base64url.unescape(program.args[0]), 'base64');
var token = jwt.encode(payload, secret);

console.log(token);
process.exit(0);
